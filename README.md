# genmon-net_monitor



The following script in bash, is a cinnamon panel applet for Linux Mint, that provides network monitoring (upload download speed , and bars).It works with the RunCommand Applet, for Linux Mint Cinnamon Desktop Environment.

On hover over the applet, this tooltip is shown:

![image 2](screenshot2.png)

Clicking on the panel applet opens cinnamon-settings network.

## INSTRUCTIONS

- Clone the repo and change directory to `network-monitor-with-bars` directory:

```
git clone https://gitlab.com/christosangel/network-monitor-with-bars.git&&cd network-monitor-with-bars/

```



-  Make this file executable with the following command in terminal:

```
chmod +x genmon-network.sh
```

* Make sure you download the **CommandRunner Applet** in cinnamon panel:

 * Right click on the panel you want to present the network monitor

 * Select **Applets**

 * Select **Download** tab, select **CommandRunner** and download it

 * Back on the **Manage** tab, press the CommandRunner **Configure** button.

 * In the **Command** field, put : `/homer/user'sname/path-to-directory/network-monitor-with-bars/genmon-network.sh`
 * In the **Run Interval** field, put: 1 second
 * Hit **Apply**
 * You are good to go.
 
 **NOTICE 1:**
  *Depending your internet download and upload maximum speed, you may want to change the numbers on the two lines 34 35  accordingly, in order for the bars to show accurately:*
 
```

    BUP=$(( $UP/12 ))
    
BDOWN=$(( $DOWN/80 ))
```


 **NOTICE 2:** *Results may depend on the screen resolution, and the panel height, in pixels. Feel free to change these parameters in order to get an optimal result.*
 
 
 
