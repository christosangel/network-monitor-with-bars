#! /bin/bash
#  _   _          _                               _
# | \ | |   ___  | |_  __      __   ___    _ __  | | __
# |  \| |  / _ \ | __| \ \ /\ / /  / _ \  | '__| | |/ /
# | |\  | |  __/ | |_   \ V  V /  | (_) | | |    |   <
# |_| \_|  \___|  \__|   \_/\_/    \___/  |_|    |_|\_\
#   written by Christos Angelopoulos September 2021

#Defining Active network interface
INTERFACE="$(ip addr | grep "state UP"|awk '{print $2}'|sed 's/\://')"
IP="$(hostname -I|sed 's/ .*$//')"
#temp cache
ucache=/tmp/unetbarcache
dcache=/tmp/bnetbarcache
#defining statistics, bytes received / tramsmitted respectively
ustat="$(cat /sys/class/net/$INTERFACE/statistics/tx_bytes)"
dstat="$(cat /sys/class/net/$INTERFACE/statistics/rx_bytes)"
if [ ! -f $ucache ]
then
 echo "$ustat" > "$ucache"
sleep 1
ustat="$(cat /sys/class/net/$INTERFACE/statistics/tx_bytes)"
fi
if [ ! -f $dcache ]
then
 echo "$dstat" > "$dcache"
sleep 1
dstat="$(cat /sys/class/net/$INTERFACE/statistics/rx_bytes)"
fi
uold=$(cat "$ucache")
dold=$(cat "$dcache")
UP=$(( $ustat - $uold )); UP=$(( $UP /1024 ))
DOWN=$(( $dstat - $dold )) ; DOWN=$(( $DOWN / 1024 ))
#depending your internet download and upload maximum speed, you may want to change the numbers on the two following lines accordingly
BUP=$(( $UP/12 ))
BDOWN=$(( $DOWN/70 ))
if [ $BDOWN -gt 9 ];then BDOWN=9;fi
if [ $BUP -gt 9 ];then BUP=9;fi
mes1="$(echo "$DOWN kB        "|cut -b -6)"
mes3="$(echo "$UP kB         "|cut -b -6)"
echo "$ustat" > "$ucache"
echo "$dstat" > "$dcache"
if [[ $DESKTOP_SESSION == "cinnamon"* ]]
then
	echo "<xml>
	<appsettings>
	<tooltip>Network Monitor
	──────────────
	Interface :$INTERFACE
	──────────────
	IP: $IP
	──────────────
		 Download : $DOWN kB/s
	──────────────
		 Upload :      $UP kB/s</tooltip><clickaction>cinnamon-settings network & nm-connection-editor</clickaction>
	</appsettings>
	<item>
	<type>box</type>
	<attr>
	<vertical>1</vertical>
	</attr>
	<item>
	<type>box</type>
	<attr>
	<vertical>0</vertical>
	<xalign>0</xalign>
	</attr>
	<item>
	<type>box</type>
	<attr>
	<vertical>1</vertical>
	<xalign>0</xalign>
	</attr>
	<item>
	<type>text</type>
	<value>$mes1</value>
	<attr><xalign>0</xalign><style>padding: 0px;font-size: 9pt;font-weight: bold;color:rgb(255,132,41)</style></attr>
	</item>
	<item>
	<type>text</type>
	<value>$mes3</value>
	<attr><xalign>0</xalign><style>padding: 0px;font-size: 9pt;font-weight: bold;color:rgb(108,204,63)</style></attr>
	</item>
	</item>
	</item>
	<item>
	<type>text</type>
	<value>XXXXXXXXXXXXXXXXΧΧΧXXXXXXXXX</value>
	<attr><xalign>0</xalign><yalign>2</yalign><style>font-size: 2pt;color:rgb(88,88,88)</style></attr>
	</item>
	</item>
	<item>
	<type>box</type>
	<attr>
	<vertical>1</vertical>
	<xalign>0</xalign>
	<yalign>2</yalign>
	<style>padding: 0px</style>
	</attr>">/tmp/netxml
	#loop to decide which download levels are on
	i=9
	while [ $i -ge 0 ]
	do
		if [ $i -lt $BDOWN ];then 	BAR_0_[$i]="_██████_";else 	BAR_0_[$i]="        ";	fi
		echo "<item>
		<type>text</type>
		<value>${BAR_0_[$i]}</value>
		<attr><xalign>0</xalign><style>font-size: 1pt;font-weight: bold;color:rgb(255,132,41);padding: 0px</style></attr>
		</item>">>/tmp/netxml
		((i--))
	done
	echo "<item>
	<type>text</type>
	<value>_██████_</value>
	<attr><xalign>0</xalign><style>font-size: 1pt;font-weight: bold;color:rgb(255,132,41);padding: 0px</style></attr>
	</item>
	</item>
	<item>
	<type>box</type>
	<attr>
	<vertical>1</vertical>
	<xalign>0</xalign>
	<yalign>2</yalign>
	<style>padding: 0px</style>
	</attr>	">>/tmp/netxml
	i=9
	while [ $i -ge 0 ]
	do
		if [ $i -lt $BUP ];then 	BAR_1_[$i]="_██████_";else 	BAR_1_[$i]="        ";	fi
		echo "<item>
			<type>text</type>
		<value>${BAR_1_[$i]}</value>
		<attr><xalign>0</xalign><style>font-size: 1pt;font-weight: bold;color:rgb(108,204,63);padding: 0px</style></attr>
		</item>">>/tmp/netxml
		((i--))
	done
	echo "<item>
	<type>text</type>
	<value>_██████_</value>
	<attr><xalign>0</xalign><style>font-size: 1pt;font-weight: bold;color:rgb(108,204,63);padding: 0px</style></attr>
	</item>">>/tmp/netxml
	echo "</item></xml>">>/tmp/netxml
	cat /tmp/netxml
elif [[ $DESKTOP_SESSION == "xfce" ]]
then
		echo -e "		<tool>Network Monitor
──────────────
Interface :$INTERFACE
──────────────
IP: $IP
──────────────
Download : $DOWN kB/s
──────────────
Upload :      $UP kB/s</tool><click>nm-connection-editor</click><img>$HOME/git/genmon-network/nordx2/$BDOWN$BUP</img><txt><span gravity='west' foreground='#a3be8c' font='9' line_height='0.7'><b>🠣 </b><tt>$mes1</tt>\n</span><span gravity='west'foreground='#d08770' font='9'><b>🠡 </b><tt>$mes3</tt></span></txt>"
#Upload :      $UP kB/s</tool><click>cinnamon-settings network & nm-connection-editor</click><img>$HOME/git/network-monitor-with-bars/x2/$BDOWN$BUP</img><txt><span foreground='lime' font='10' line_height='0.7'> <b>  $mes1   </b>\n</span><span foreground='orange' font='10'  ><b>  $mes3  </b></span><span foreground='orange' font='12'  >\nXXXXXXXX</span></txt>"
fi
